import argparse
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier

# Парсер аргументів
def build_arg_parser():
    parser = argparse.ArgumentParser(description="Classify data using Ensemble Learning techniques")
    parser.add_argument('--classifier-type',
                        dest='classifier_type', required=True,
                        choices=['rf', 'erf'], help="Type of classifier to use; can be either 'rf' or 'erf'")
    return parser

# Визуалізація класифікатора
def visualize_classifier(classifier, X, y, title):
    plt.figure()
    plt.scatter(X[:, 0], X[:, 1], s=30, c=y, cmap=plt.cm.Paired, edgecolor='k')
    plt.title(title)

if __name__ == '__main__':
    # Вилучення вхідних аргументів
    args = build_arg_parser().parse_args()
    classifier_type = args.classifier_type

    # Завантаження вхідних даних
    input_file = 'data_random_forests.txt'
    data = np.loadtxt(input_file, delimiter=',')
    X, y = data[:, :-1], data[:, -1]

    # Розбивка даних на навчальний та тестовий набори
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=5)

    # Параметри класифікатора
    params = {'n_estimators': 100, 'max_depth': 4, 'random_state': 0}

    # Класифікатор на основі випадкового або гранично випадкового лісу
    if classifier_type == 'rf':
        classifier = RandomForestClassifier(**params)
    else:
        classifier = ExtraTreesClassifier(**params)

    # Навчання класифікатора
    classifier.fit(X_train, y_train)

    # Візуалізація навчального набору даних
    visualize_classifier(classifier, X_train, y_train, 'Training dataset')

    # Візуалізація тестового набору даних
    visualize_classifier(classifier, X_test, y_test, 'Test dataset')

    # Обчислення результатів на тестовому наборі даних
    y_test_pred = classifier.predict(X_test)

    # Перевірка роботи класифікатора
    class_names = ['Class-0', 'Class-1', 'Class-2']
    print("\n" + "#" * 40)
    print("\nClassifier performance on training dataset \n")
    print(classification_report(y_train, classifier.predict(X_train), target_names=class_names))
    print("#" * 40 + "\n")
    print("#" * 40)
    print("\nClassifier performance on test dataset \n")
    print(classification_report(y_test, y_test_pred, target_names=class_names))
    print("#" * 40 + "\n")

    # Обчислення параметрів довірливості для тестових точок
    test_datapoints = np.array([[5, 5], [3, 6], [6, 4], [7, 2], [4, 4], [5, 2]])
    print('\nConfidence measure:')
    for datapoint in test_datapoints:
        probabilities = classifier.predict_proba([datapoint])[0]
        predicted_class = 'Class-' + str(np.argmax(probabilities))
        print('\nDatapoint:', datapoint)
        print('Predicted class:', predicted_class)

    # Візуалізація тестових точок даних
    visualize_classifier(classifier, test_datapoints, [0] * len(test_datapoints), 'Тестові точки даних')
    plt.show()

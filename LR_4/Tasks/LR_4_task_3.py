import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.model_selection import GridSearchCV
from utilities import visualize_classifier

# Завантаження даних з файлу
input_file = 'data_random_forests.txt'
data = np.loadtxt(input_file, delimiter=',')
X, y = data[:, :-1], data[:, -1]

# Розбиття даних на навчальний та тестовий набори
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=5)

# Визначення сітки значень параметрів для сіткового пошуку
parameter_grid = {
    'n_estimators': [100],
    'max_depth': [2, 4, 7, 12, 16]
}

# Визначення метрики для сіткового пошуку
scoring_metric = 'accuracy'

# Створення класифікатору ExtraTreesClassifier
classifier = ExtraTreesClassifier(random_state=0)

# Створення об'єкта GridSearchCV для сіткового пошуку
grid_search = GridSearchCV(classifier, param_grid=parameter_grid, scoring=scoring_metric, cv=5)

# Пошук оптимальних параметрів за допомогою сіткового пошуку
grid_search.fit(X_train, y_train)

# Виведення результатів сіткового пошуку
print("Grid scores for the parameter grid:")
for i, params in enumerate(grid_search.cv_results_['params']):
    avg_score = grid_search.cv_results_['mean_test_score'][i]
    print(params, '-->', round(avg_score, 3))

print("\nBest parameters:", grid_search.best_params_)

# Побудова та навчання класифікатора з оптимальними параметрами
best_classifier = ExtraTreesClassifier(n_estimators=grid_search.best_params_['n_estimators'],
                                      max_depth=grid_search.best_params_['max_depth'],
                                      random_state=0)
best_classifier.fit(X_train, y_train)

# Візуалізація класифікатора
visualize_classifier(best_classifier, X_train, y_train)

# Передбачення та візуалізація результатів для тестового набору даних
y_test_pred = best_classifier.predict(X_test)
visualize_classifier(best_classifier, X_test, y_test)

# Обчислення показників ефективності класифікатора
class_names = ['Class-0', 'Class-1', 'Class-2']
print("\nPerformance report on test dataset:\n")
print(classification_report(y_test, y_test_pred, target_names=class_names))

import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression

# Створення випадкових даних
m = 100
X = 6 * np.random.rand(m, 1) - 4
y = 0.5 * X ** 2 + X + 2 + np.random.randn(m, 1)

# Використовуємо клас PolynomialFeatures для додавання поліноміальних ознак
poly_features = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly_features.fit_transform(X)

# Підгонка моделі LinearRegression до розширених даних
lin_reg = LinearRegression()
lin_reg.fit(X_poly, y)

# Виведення значень коефіцієнтів полінома
intercept = lin_reg.intercept_
coefficients = lin_reg.coef_

print("Коефіцієнт перехоплення (intercept):", intercept)
print("Коефіцієнти регресії для X^1 та X^2:", coefficients)

# Побудова графіка
X_new = np.linspace(-4, 2, 100).reshape(100, 1)
X_new_poly = poly_features.transform(X_new)
y_new = lin_reg.predict(X_new_poly)

plt.scatter(X, y, label='Дані', c='blue')
plt.plot(X_new, y_new, 'r-', label='Поліноміальна регресія', linewidth=2)
plt.xlabel('X')
plt.ylabel('y')
plt.legend(loc='upper left')
plt.title('Поліноміальна регресія')
plt.grid(True)
plt.show()

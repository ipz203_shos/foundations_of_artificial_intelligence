# Імпорт необхідних пакетів
import pickle
import numpy as np
from sklearn import linear_model
import sklearn.metrics as sm
import matplotlib.pyplot as plt

# Завантаження даних з текстового файлу
input_file = 'data_regr_3.txt'
data = np.loadtxt(input_file, delimiter=',')
X, y = data[:, :-1], data[:, -1]

# Розділка даних на навчальний та тестовий набори
num_training = int(0.8 * len(X))
num_test = len(X) - num_training
X_train, y_train = X[:num_training], y[:num_training]
X_test, y_test = X[num_training:], y[num_training:]

# Створення об'єкта лінійного регресора і навчання на навчальних даних
regressor = linear_model.LinearRegression()
regressor.fit(X_train, y_train)

# Прогнозування результатів для тестового набору даних
y_test_pred = regressor.predict(X_test)

# Побудова графіка
plt.scatter(X_test, y_test, color='green', label='Справжні значення')
plt.plot(X_test, y_test_pred, color='black', linewidth=4, label='Прогнозовані значення')
plt.xticks(())
plt.yticks(())
plt.legend(loc='best')
plt.title('Графік справжніх і прогнозованих значень')
plt.show()

# Обчислення метричних параметрів
mean_absolute_error = sm.mean_absolute_error(y_test, y_test_pred)
mean_squared_error = sm.mean_squared_error(y_test, y_test_pred)
median_absolute_error = sm.median_absolute_error(y_test, y_test_pred)
explained_variance_score = sm.explained_variance_score(y_test, y_test_pred)
r2_score = sm.r2_score(y_test, y_test_pred)

# Виведення результатів оцінки
print("Linear regressor performance:")
print("Mean absolute error =", round(mean_absolute_error, 2))
print("Mean squared error =", round(mean_squared_error, 2))
print("Median absolute error =", round(median_absolute_error, 2))
print("Explained variance score =", round(explained_variance_score, 2))
print("R2 score =", round(r2_score, 2))

# Збереження моделі у файлі
output_model_file = 'model_2.pkl'
with open(output_model_file, 'wb') as f:
    pickle.dump(regressor, f)

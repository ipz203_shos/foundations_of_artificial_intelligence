import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split


def plot_learning_curves(model, X, y):
    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, ran-dom_state=42)

    train_errors, val_errors = [], []
    for m in range(1, len(X_train)):
        model.fit(X_train[:m], y_train[:m])
        y_train_predict = model.predict(X_train[:m])
        y_val_predict = model.predict(X_val)
        train_errors.append(mean_squared_error(y_train_predict, y_train[:m]))
        val_errors.append(mean_squared_error(y_val_predict, y_val))

    plt.plot(np.sqrt(train_errors), "r-+", linewidth=2, label="навчальний набір")
    plt.plot(np.sqrt(val_errors), "b-", linewidth=3, label="валідаційний набір")
    plt.xlabel("Розмір навчального набору")
    plt.ylabel("RMSE")
    plt.legend()


# Створення випадкових даних
m = 100
X = 6 * np.random.rand(m, 1) - 4
y = 0.5 * X ** 2 + X + 2 + np.random.randn(m, 1)

# Створення моделі лінійної регресії
lin_reg = LinearRegression()

# # Створення поліноміальної моделі 2-го ступеня
# polynomial_regression = Pipeline([
#     ("poly_features", PolynomialFeatures(degree=2, include_bias=False)),
#     ("lin_reg", LinearRegression()),
# ])
#
# # Створення поліноміальної моделі 10-го ступеня
# polynomial_regression = Pipeline([
#     ("poly_features", PolynomialFeatures(degree=10, include_bias=False)),
#     ("lin_reg", LinearRegression()),
#])

# Виклик функції для побудови кривих навчання
plot_learning_curves(lin_reg, X, y)

# # Виклик функції для побудови кривих навчання
# plot_learning_curves(polynomial_regression, X, y)

plt.title("Криві навчання для лінійної регресії")
plt.show()

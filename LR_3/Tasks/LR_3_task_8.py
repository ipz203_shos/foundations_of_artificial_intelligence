import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.datasets import load_iris

# Завантаження набору даних Iris
iris = load_iris()
X = iris.data  # Ознаки
y = iris.target  # Мітки класів

# Створення об'єкту KMeans з параметрами ініціалізації
kmeans = KMeans(n_clusters=3, init='k-means++', n_init=10, max_iter=300, random_state=0)

# Навчання моделі KMeans
kmeans.fit(X)

# Отримання міток кластерів для кожного зразка
y_kmeans = kmeans.predict(X)

# Відображення результатів кластеризації
plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, s=50, cmap='viridis')
centers = kmeans.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)
plt.xlabel('Довжина чашолистка')
plt.ylabel('Ширина чашолистка')
plt.title('Кластеризація K-середніх для Iris')
plt.show()

import numpy as np
import neurolab as nl

# Ш О С
target = [
    [1, 1, 0, 0, 0, 1, 1, 1, 0],
    [0, 1, 1, 0, 1, 0, 1, 0, 1],
    [0, 1, 1, 0, 0, 0, 1, 1, 1],
]

chars = ["Ш", "О", "С"]
target = np.asfarray(target)
target[target == 0] = -1

# Створення та навчання мережі
net = nl.net.newhop(target)

output = net.sim(target)
print("Test on train samples:")
for i in range(len(target)):
    print(chars[i], (output[i] == target[i]).all())

# Тестування для Ш
print("\nTest on defaced Ш:")
test_Ш = np.asfarray([1, 0, 0, 0, 0, 1, 1, 1, 0])
test_Ш[test_Ш == 0] = -1

out_Ш = net.sim([test_Ш])
print((out_Ш[0] == target[0]).all(), "Sim. steps", len(net.layers[0].outs))

# Тестування для О
print("\nTest on defaced О:")
test_О = np.asfarray([0, 1, 1, 0, 1, 0, 1, 0, 1])
test_О[test_О == 0] = -1

out_О = net.sim([test_О])
print((out_О[0] == target[1]).all(), "Sim. steps", len(net.layers[0].outs))

# Тестування для С
print("\nTest on defaced С:")
test_С = np.asfarray([0, 1, 1, 0, 0, 0, 1, 1, 1])
test_С[test_С == 0] = -1

out_С = net.sim([test_С])
print((out_С[0] == target[2]).all(), "Sim. steps", len(net.layers[0].outs))

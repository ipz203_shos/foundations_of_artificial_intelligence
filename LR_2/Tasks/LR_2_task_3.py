import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import scatter_matrix
from sklearn.datasets import load_iris

# Завантажте дані
iris_dataset = load_iris()

# Створимо DataFrame з даними та назвами ознак
iris_df = pd.DataFrame(data=iris_dataset.data, columns=iris_dataset.feature_names)

# Додамо стовпець з мітками класів (сортами квітів)
iris_df['target'] = iris_dataset.target

# Матриця діаграм розсіювання
scatter_matrix(iris_df, c=iris_df['target'], figsize=(15, 15), marker='o', hist_kwds={'bins': 20}, alpha=0.8, cmap='viridis')

# Встановимо заголовок для всієї матриці діаграм розсіювання
plt.suptitle("Матриця діаграм розсіювання для Iris Dataset")

# Виведемо графік
plt.show()
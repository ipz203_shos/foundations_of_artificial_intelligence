import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split, cross_val_score, StratifiedKFold
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

# Завантажуємо дані з датасету Iris
iris = load_iris()
X = iris.data
y = iris.target

# Розділення даних на навчальну та контрольну вибірки
X_train, X_validation, Y_train, Y_validation = train_test_split(
    X, y, test_size=0.20, random_state=1
)

# Список моделей
models = []
models.append(("LR", LogisticRegression(solver="liblinear", multi_class="ovr")))
models.append(("LDA", LinearDiscriminantAnalysis()))
models.append(("KNN", KNeighborsClassifier()))
models.append(("CART", DecisionTreeClassifier()))
models.append(("NB", GaussianNB()))
models.append(("SVM", SVC(gamma="auto")))

# Оцінюємо моделі на кожній ітерації
results = []
names = []
for name, model in models:
    kfold = StratifiedKFold(n_splits=10, random_state=1, shuffle=True)
    cv_results = cross_val_score(model, X_train, Y_train, cv=kfold, scoring="accuracy")
    results.append(cv_results)
    names.append(name)
    print("%s: %f (%f)" % (name, cv_results.mean(), cv_results.std()))

# Порівняння алгоритмів
plt.boxplot(results, labels=names)
plt.title("Algorithm Comparison")
plt.show()

# Створюємо прогноз на контрольній вибірці за найкращою моделлю (SVM у цьому випадку)
best_model = SVC(gamma="auto")
best_model.fit(X_train, Y_train)
predictions = best_model.predict(X_validation)

# Оцінюємо прогноз
print(accuracy_score(Y_validation, predictions))
print(confusion_matrix(Y_validation, predictions))
print(classification_report(Y_validation, predictions))

# Нові дані для класифікації (за вашими значеннями)
X_new = np.array([[5.0, 2.9, 1.0, 0.2]])
# Зробіть прогноз для нових даних
prediction = best_model.predict(X_new)
# Виведіть результат прогнозу
print("Прогноз: {}".format(prediction))

import numpy as np
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.linear_model import RidgeClassifier
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import cohen_kappa_score
from sklearn.metrics import matthews_corrcoef
import seaborn as sns
import matplotlib.pyplot as plt
from io import BytesIO

iris = load_iris()
X, y = iris.data, iris.target

# Розділення даних на навчальний і тестовий набори
Xtrain, Xtest, ytrain, ytest = train_test_split(X, y, test_size=0.3, random_state=0)

# Налаштування класифікатора Ridge
clf = RidgeClassifier(tol=1e-2, solver="sag")

# Навчання класифікатора
clf.fit(Xtrain, ytrain)

# Предикція на тестових даних
ypred = clf.predict(Xtest)

# Вивід налаштувань класифікатора Ridge
print("Налаштування класифікатора Ridge:")
print("tol (tolerance):", clf.tol)
print("solver (solver method):", clf.solver)

# Вивід показників якості класифікації
accuracy = np.round(metrics.accuracy_score(ytest, ypred), 4)
precision = np.round(metrics.precision_score(ytest, ypred, average='weighted'), 4)
recall = np.round(metrics.recall_score(ytest, ypred, average='weighted'), 4)
f1_score = np.round(metrics.f1_score(ytest, ypred, average='weighted'), 4)
cohen_kappa = np.round(cohen_kappa_score(ytest, ypred), 4)
matthews_corr = np.round(matthews_corrcoef(ytest, ypred), 4)

print('Accuracy:', accuracy)
print('Precision:', precision)
print('Recall:', recall)
print('F1 Score:', f1_score)
print('Cohen Kappa Score:', cohen_kappa)
print('Matthews Corrcoef:', matthews_corr)

# Classification Report
classification_report = metrics.classification_report(ypred, ytest)
print('\t\tClassification Report:\n', classification_report)

# Матриця плутанини і теплокарта
mat = confusion_matrix(ytest, ypred)
sns.heatmap(mat.T, square=True, annot=True, fmt='d', cbar=False)
plt.xlabel('true label')
plt.ylabel('predicted label')
plt.savefig("Confusion.jpg")

# Збереження SVG у фейковому об'єкті файлу.
f = BytesIO()
plt.savefig(f, format="svg")

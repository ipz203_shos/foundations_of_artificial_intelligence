import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.svm import SVC

# Вхідний файл, який містить дані
input_file = 'income_data.txt'

# Читання даних та підготовка для класифікації
X = []
y = []
count_class1 = 0
count_class2 = 0
max_datapoints = 25000
with open(input_file, "r") as f:
    for line in f.readlines():
        if count_class1 >= max_datapoints and count_class2 >= max_datapoints:
            break
        if "?" in line:
            continue
        data = line[:-1].split(", ")
        if data[-1] == "<=50K" and count_class1 < max_datapoints:
            X.append(data)
            count_class1 += 1
        elif data[-1] == ">50K" and count_class2 < max_datapoints:
            X.append(data)
            count_class2 += 1

# Перетворення на масив numpy
X = np.array(X)

# Перетворення рядкових даних на числові
label_encoder = []
X_encoded = np.empty(X.shape)

for i, item in enumerate(X[0]):
    if item.isdigit():
        X_encoded[:, i] = X[:, i]
    else:
        label_encoder.append(preprocessing.LabelEncoder())
        X_encoded[:, i] = label_encoder[-1].fit_transform(X[:, i])
X = X_encoded[:, :-1].astype(int)
y = X_encoded[:, -1].astype(int)

# Нелінійний класифікатор SVM з поліноміальним ядром
# classifier = OneVsOneClassifier(SVC(kernel='poly', degree=8, random_state=0))
classifier = SVC(kernel='poly', degree=8, random_state=0)

# Розділення даних на навчальний та тестовий набори (80/20)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

# Навчання класифікатора
classifier.fit(X_train, y_train)

# Прогнозування результату для тестових даних
y_test_pred = classifier.predict(X_test)

# Обчислення F-міри для SVМ-класифікатора
from sklearn.metrics import f1_score
f1 = f1_score(y_test, y_test_pred, average="weighted")
print("F1 score: " + str(round(100 * f1, 2)) + "%")

# Тестова точка даних для класифікації
input_data = ['37', 'Private', '215646', 'HS-grad', '9', 'Never-married', 'Handlers-cleaners', 'Not-in-family', 'White',
              'Male', '0', '0', '40', 'United-States']

# Кодування тестової точки даних
input_data_encoded = [-1] * len(input_data)
count = 0
for i, item in enumerate(input_data):
    if item.isdigit():
        input_data_encoded[i] = int(input_data[i])
    else:
        input_data_encoded[i] = int(label_encoder[count].transform([input_data[i]])[0])
        count += 1
input_data_encoded = np.array(input_data_encoded).reshape(1, -1)

# Використання класифікатора для кодованої точки даних та виведення результату
predicted_class = classifier.predict(input_data_encoded)
predicted_label = label_encoder[-1].inverse_transform(predicted_class)[0]
print("Predicted class: " + predicted_label)

# Обчислення акуратності
accuracy = accuracy_score(y_test, y_test_pred)
print("Accuracy:" + str(round(100 * accuracy, 2)) + "%")

# Обчислення точності
precision = precision_score(y_test, y_test_pred, average="weighted")
print("Precision:" + str(round(100 * precision, 2)) + "%")

# Обчислення повноти
recall = recall_score(y_test, y_test_pred, average="weighted")
print("Recall:" + str(round(100 * recall, 2)) + "%")


# # Нелінійний класифікатор SVM з поліноміальним ядром
# poly_svc = SVC(kernel='poly', degree=3, random_state=0)
# poly_svc.fit(X_train, y_train)



# # Оцінка якості для поліноміального ядра
# y_pred_poly = poly_svc.predict(X_test)
# report_poly = classification_report(y_test, y_pred_poly)
# print("Classification Report (Polynomial Kernel):\n", report_poly)
#
# # Нелінійний класифікатор SVM з гаусовим ядром (RBF)
# rbf_svc = SVC(kernel='rbf', gamma='auto', random_state=0)
# rbf_svc.fit(X_train, y_train)
#
# # Оцінка якості для гаусового ядра (RBF)
# y_pred_rbf = rbf_svc.predict(X_test)
# report_rbf = classification_report(y_test, y_pred_rbf)
# print("Classification Report (RBF Kernel):\n", report_rbf)
#
# # Нелінійний класифікатор SVM з сигмоїдальним ядром
# sigmoid_svc = SVC(kernel='sigmoid', gamma='auto', random_state=0)
# sigmoid_svc.fit(X_train, y_train)
#
# # Оцінка якості для сигмоїдального ядра
# y_pred_sigmoid = sigmoid_svc.predict(X_test)
# report_sigmoid = classification_report(y_test, y_pred_sigmoid)
# print("Classification Report (Sigmoid Kernel):\n", report_sigmoid)
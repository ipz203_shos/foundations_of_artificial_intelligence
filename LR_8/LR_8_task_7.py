import numpy as np
import cv2

# Завантаження зображення
img = cv2.imread('coins_2.jpg')

# Конвертуємо зображення в градації сірого
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Бінаризація за допомогою OTSU
_, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

# Видалення шуму за допомогою морфологічного відкриття
kernel = np.ones((3, 3), np.uint8)
opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)

# Визначення областей переднього плану та фону за допомогою відстані
dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
ret, sure_fg = cv2.threshold(dist_transform, 0.7 * dist_transform.max(), 255, 0)

# Визначення області фону
sure_bg = cv2.dilate(opening, kernel, iterations=3)

# Визначення невідомої області
sure_fg = np.uint8(sure_fg)
unknown = cv2.subtract(sure_bg, sure_fg)

# Маркування міток
ret, markers = cv2.connectedComponents(sure_fg)
markers = markers + 1
markers[unknown == 255] = 0

# Вододіл
cv2.watershed(img, markers)
img[markers == -1] = [0, 255, 255]  # Позначення границь монет жовтим кольором

# Збільшення товщини границь монет
img = cv2.copyMakeBorder(img, 3, 3, 3, 3, cv2.BORDER_CONSTANT, value=[0, 0, 0])

# Відображення результату
cv2.imshow("Segmented Coins", img)
cv2.waitKey(0)

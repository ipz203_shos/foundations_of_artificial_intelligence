import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score, classification_report

# Зчитуємо дані з файлу
data = np.loadtxt("data_multivar_nb.txt", delimiter=",")

# Розділяємо дані на ознаки X та цільові значення y
X = data[:, :-1]
y = data[:, -1]

# Розділяємо дані на навчальний та тестовий набори
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Навчаємо модель SVM
svm_model = SVC(kernel='linear')
svm_model.fit(X_train, y_train)

# Навчаємо модель наївного байєсівського класифікатора
nb_model = GaussianNB()
nb_model.fit(X_train, y_train)

# Передбачення для тестового набору
y_pred_svm = svm_model.predict(X_test)
y_pred_nb = nb_model.predict(X_test)

# Розраховуємо показники якості
accuracy_svm = accuracy_score(y_test, y_pred_svm)
accuracy_nb = accuracy_score(y_test, y_pred_nb)

classification_report_svm = classification_report(y_test, y_pred_svm)
classification_report_nb = classification_report(y_test, y_pred_nb)

# Виводимо результати
print("Модель SVM:")
print(f"Точність: {accuracy_svm:.2f}")
print("Звіт про класифікацію:")
print(classification_report_svm)

print("Модель наївного байєсівського класифікатора:")
print(f"Точність: {accuracy_nb:.2f}")
print("Звіт про класифікацію:")
print(classification_report_nb)
